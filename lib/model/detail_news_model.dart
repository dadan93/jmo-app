class DetailNewsModel {
  final String? title;
  final String? description;
  final String? urlToImage;

  DetailNewsModel({this.title, this.description, this.urlToImage});
}
