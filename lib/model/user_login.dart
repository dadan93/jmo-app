class UserLogin {
  final String? name;
  final String? email;
  final String? phone;
  final String? address;
  final String? profilePicture;

  UserLogin(
      {this.name, this.email, this.phone, this.address, this.profilePicture});
}
