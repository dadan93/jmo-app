// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

import 'package:jmo_app/model/links.dart';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  User({
    this.status,
    this.token,
    this.refreshToken,
    this.user,
  });

  String? status;
  String? token;
  String? refreshToken;
  UserClass? user;

  factory User.fromJson(Map<String, dynamic> json) => User(
        status: json["status"],
        token: json["token"],
        refreshToken: json["refresh_token"],
        user: UserClass.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "token": token,
        "refresh_token": refreshToken,
        "user": user!.toJson(),
      };
}

class UserClass {
  UserClass({
    this.uid,
    this.fullName,
    this.username,
    this.address,
    this.idCardNumber,
    this.shortBio,
    this.avatarUrl,
    this.email,
    this.phone,
    this.authKey,
    this.status,
    this.pinSet,
    this.createdAt,
    this.updatedAt,
    this.links,
  });

  String? uid;
  String? fullName;
  String? username;
  String? address;
  String? idCardNumber;
  String? shortBio;
  String? avatarUrl;
  String? email;
  String? phone;
  String? authKey;
  int? status;
  int? pinSet;
  DateTime? createdAt;
  DateTime? updatedAt;
  Links? links;

  factory UserClass.fromJson(Map<String, dynamic> json) => UserClass(
        uid: json["uid"],
        fullName: json["full_name"],
        username: json["username"],
        address: json["address"],
        idCardNumber: json["id_card_number"],
        shortBio: json["short_bio"],
        avatarUrl: json["avatar_url"],
        email: json["email"],
        phone: json["phone"],
        authKey: json["auth_key"],
        status: json["status"],
        pinSet: json["pin_set"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        links: Links.fromJson(json["links"]),
      );

  Map<String, dynamic> toJson() => {
        "uid": uid,
        "full_name": fullName,
        "username": username,
        "address": address,
        "id_card_number": idCardNumber,
        "short_bio": shortBio,
        "avatar_url": avatarUrl,
        "email": email,
        "phone": phone,
        "auth_key": authKey,
        "status": status,
        "pin_set": pinSet,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "links": links!.toJson(),
      };
}
