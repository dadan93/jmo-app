import 'package:dio/dio.dart';
import 'package:jmo_app/model/user.dart';
import 'package:jmo_app/utils/auth.dart';

class CHttp {
  const CHttp({this.baseURL, this.auth});

  final String? baseURL;
  final Auth? auth;

  Future<Dio> getClient() async {
    Dio dio = Dio();
    dio.options.baseUrl = baseURL!;

    if (auth == null) {
      return dio;
    }

    final bool? isLogin = await auth!.isLoggedIn();
    await auth!.isLoggedIn();

    if (!isLogin!) {
      return dio;
    }

    User? user = await auth!.refreshToken();

    if (user == null) {
      return dio;
    }

    // print(user.token);
    dio.interceptors.clear();
    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (options, handler) {
        options.headers["Authorization"] = "Bearer ${user!.token}";
        return handler.next(options); //continue
      },
      onResponse: (response, handler) {
        // Do something with response data
        return handler.next(response); // continue
      },
      onError: (DioError err, handler) async {
        if (err.response?.statusCode == 401) {
          RequestOptions options = err.response!.requestOptions;
          final opts = Options(method: options.method);
          user = await auth!.refreshToken();
          options.headers["Authorization"] = "Bearer ${user!.token}";
          try {
            final response = await dio.request(options.path,
                options: opts,
                data: options.data,
                queryParameters: options.queryParameters);
            handler.resolve(response);
          } on DioError catch (error) {
            print("error handler");
            return handler.next(error); // or handler.reject(error);
          }
        } else {
          print("error handler $err");
          return handler.next(err);
        }
      },
    ));
    return dio;
  }
}
