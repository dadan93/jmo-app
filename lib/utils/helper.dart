import 'dart:math';

import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

class JMOelper {
  static String formatCurrency(double number) {
    final NumberFormat fmt = NumberFormat.currency(locale: 'id', symbol: 'Rp ');
    String s = fmt.format(number);
    String format = s.toString().replaceAll(RegExp(r"([,]*00)(?!.*\d)"), "");
    return format;
  }

  static String formatDecimal(double number) {
    final NumberFormat fmt = NumberFormat.decimalPattern();
    String s = fmt.format(number);
    String format = s.toString().replaceAll(RegExp(r"([,]*00)(?!.*\d)"), "");
    return format;
  }

  static String formatDate(DateTime dateTime) {
    initializeDateFormatting("id");
    return DateFormat.yMMMMEEEEd("id").format(dateTime);
  }

  static String formatDateShort(DateTime dateTime) {
    initializeDateFormatting("id");
    return DateFormat.yMd("id").format(dateTime);
  }

  static String formatDateShort2(DateTime dateTime) {
    initializeDateFormatting("en");
    return DateFormat.yMMM("en").format(dateTime);
  }

  static String formatDateShort3(DateTime dateTime) {
    final f = DateFormat('MMM dd, yyyy, kk:mm');
    return f.format(dateTime);
  }

  static String formatDateShort4(DateTime dateTime) {
    final f = DateFormat('MMM dd, yyyy,');
    return f.format(dateTime);
  }

  static String formatDateShort5(DateTime dateTime) {
    final f = DateFormat('MMM dd, yyyy');
    return f.format(dateTime);
  }

  static String formatOnlyTime(DateTime dateTime) {
    final f = DateFormat.jm();
    return f.format(dateTime);
  }

  static String formatDateMiddle(DateTime dateTime) {
    initializeDateFormatting("id");
    return DateFormat.yMMMMd("id").format(dateTime);
  }

  static DateTime formatDateLong(String dateTime) {
    initializeDateFormatting("en_US");
    return DateFormat.yMd('en_US').add_jms().parse(dateTime);
  }

  static String formatDate2(DateTime dateTime) {
    final f = DateFormat('dd/MM/yyyy');
    return f.format(dateTime);
  }

  static String formatDate3(DateTime dateTime) {
    final f = DateFormat('yyyy-MM-dd');
    return f.format(dateTime);
  }

  static String formatDate4(DateTime dateTime) {
    final f = DateFormat('yyyy-MM-dd');
    return f.format(dateTime);
  }

  static String formatDay(DateTime dateTime) {
    final f = DateFormat('EEEE, d');
    return f.format(dateTime);
  }

  static int daysBetween(DateTime from, DateTime to) {
    from = DateTime(from.year, from.month, from.day);
    to = DateTime(to.year, to.month, to.day);
    return (to.difference(from).inHours / 24).round();
  }

  static double calculateDistance(lat1, long1, lat2, long2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((long2 - long1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }
}
