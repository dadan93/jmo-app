import 'package:jmo_app/view/onboarding_screen.dart';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../view/page-home/widgets/home_widget.dart';

class Root extends StatefulWidget {
  const Root({Key? key}) : super(key: key);

  @override
  RootState createState() => RootState();
}

class RootState extends State<Root> {
  bool isHomeLoading = true;
  bool goingHome = true;
  String? name;

  Widget buildProgressIndicator() {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(),
    );
  }

  dataCheck() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString("name");
      isHomeLoading = false;
      goingHome = true;
    });
  }

  @override
  void initState() {
    dataCheck();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (!isHomeLoading) {
      if (goingHome && name != null) {
        return const HomeWidget();
      } else {
        return const OnboardingScreen();
      }
    }
    return buildProgressIndicator();
  }
}
