import 'package:dio/dio.dart';
import 'package:jmo_app/constant/constant.dart';
import 'package:jmo_app/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class BaseAuth {
  Future<User?> login(String username, String password);
  Future<User?> loadUser();
  Future<bool?> isLoggedIn();
  Future<User?> refreshToken();
  Future<User?> register(String username, String email, String fullname, String password);

  User? currentUser;
  void logout();
}

class Auth implements BaseAuth {
  @override
  User? currentUser;

  final Dio _dio = Dio();

  @override
  Future<bool?> isLoggedIn() async {
    bool isLoaded = false;
    User? user = await loadUser();
    isLoaded = user == null ? false : true;
    return isLoaded;
  }

  @override
  Future<User?> loadUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString("uid") == null) {
      currentUser = null;
    } else {
      User user = User(
          refreshToken: prefs.getString("refreshToken"),
          token: prefs.getString("token"),
          user: UserClass(
              uid: prefs.getString("uid"),
              fullName: prefs.getString("fullName"),
              username: prefs.getString("username"),
              address: prefs.getString("address"),
              idCardNumber: prefs.getString("idCardNumber"),
              shortBio: prefs.getString("shortBio"),
              avatarUrl: prefs.getString("avatarUrl"),
              email: prefs.getString("email"),
              phone: prefs.getString("phone"),
              authKey: prefs.getString("authKey"),
              status: prefs.getInt("status"),
              pinSet: prefs.getInt("pinSet")));
      currentUser = user;
    }
    return currentUser;
  }

  @override
  Future<User?> login(String username, String password) async {
    String loginUrl = "$baseUrl/user-service/login";
    try {
      Response res = await _dio.post(loginUrl, data: {"username": username, "password": password});

      User user = User.fromJson(res.data);
      currentUser = user;
      _writeData();
    } on DioError catch (err) {
      currentUser = null;
      _deleteData();
      throw err.response!.data;
    }
    return currentUser;
  }

  @override
  Future<User?> refreshToken() async {
    if (currentUser != null) {
      String refreshUrl = "$baseUrl/user-service/refresh";
      try {
        Response res = await _dio.post(refreshUrl,
            options: Options(headers: {
              "Authorization": "Bearer ${currentUser!.refreshToken}",
              "Content-Type": "application/json",
            }));

        User user = User.fromJson(res.data);
        currentUser = user;
        _writeData();
      } catch (err) {
        currentUser = null;
        _deleteData();
      }
    }
    return currentUser;
  }

  @override
  Future<User?> register(String username, String email, String fullname, String password) async {
    String registerUrl = "$baseUrl/user-service/register";
    try {
      Response res =
          await _dio.post(registerUrl, data: {"username": username, "email": email, "full_name": fullname, "role": "common.user", "password": password});

      User user = User.fromJson(res.data);
      currentUser = user;
      _writeData();
    } on DioError catch (err) {
      currentUser = null;
      _deleteData();
      throw err.response!.data;
    }
    return currentUser;
  }

  @override
  void logout() {
    _deleteData();
  }

  void _writeData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("refreshToken", currentUser!.refreshToken!);
    prefs.setString("token", currentUser!.token!);
    prefs.setString("uid", currentUser!.user!.uid!);
    prefs.setString("fullName", currentUser!.user!.fullName!);
    prefs.setString("username", currentUser!.user!.username!);
    prefs.setString("address", currentUser!.user!.address!);
    prefs.setString("idCardNumber", currentUser!.user!.idCardNumber!);
    prefs.setString("shortBio", currentUser!.user!.shortBio!);
    prefs.setString("avatarUrl", currentUser!.user!.avatarUrl!);
    prefs.setString("email", currentUser!.user!.email!);
    prefs.setString("phone", currentUser!.user!.phone!);
    prefs.setString("authKey", currentUser!.user!.authKey!);
    prefs.setInt("status", currentUser!.user!.status!);
    prefs.setInt("pinSet", currentUser!.user!.pinSet!);
  }

  void _deleteData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  Future<User?> putProfile(String uid, String fullname, String email, String phone, String address, [String avatarUrl = '']) async {
    String editProfileUrl = "$baseUrl/user-service/account/$uid";
    Map<String, dynamic> data = {"full_name": fullname, "address": address, "email": email, "phone": phone};

    if (avatarUrl.trim().isNotEmpty) {
      data.addAll({
        "avatar_url": avatarUrl,
      });
    }
    try {
      Response res = await _dio.put(editProfileUrl, data: data, options: Options(headers: {"Authorization": "Bearer ${currentUser!.token}"}));

      currentUser = User(
          refreshToken: currentUser!.refreshToken,
          token: currentUser!.token,
          user: UserClass(
            uid: currentUser!.user!.uid,
            fullName: res.data['data']['full_name'],
            email: res.data['data']['email'],
            address: res.data['data']['address'],
            phone: res.data['data']['phone'],
            avatarUrl: res.data['data']['avatar_url'],
            username: currentUser!.user!.username,
            idCardNumber: currentUser!.user!.idCardNumber,
            shortBio: currentUser!.user!.shortBio,
            authKey: currentUser!.user!.authKey,
            status: currentUser!.user!.status,
            pinSet: currentUser!.user!.pinSet,
          ));
      _writeData();
    } on DioError catch (err) {
      currentUser = null;
      _deleteData();
      throw err.response!.data;
    }
    return currentUser;
  }

  Future changePassword(String oldPasword, String newPassword, String confirmPassword) async {
    String changepswd = "$baseUrl/user-service/changepwd";
    try {
      Response res = await _dio.post(changepswd,
          data: {"old_password": oldPasword, "password1": newPassword, "password2": confirmPassword},
          options: Options(headers: {"Authorization": "Bearer ${currentUser!.token}"}));

      return res.data;
    } on DioError catch (err) {
      return err.error;
    }
  }
}
