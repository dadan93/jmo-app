import 'package:jmo_app/utils/root.dart';
import 'package:jmo_app/view/page-auth/page_login.dart';
import 'package:jmo_app/view/page-home/widgets/home_widget.dart';

import '../view/page-home/page_home.dart';

final appRoutes = {
  // page home
  '/': (context) => const Root(),
  '/login': (context) => const LoginPage(),
  '/home': (context) => const HomeWidget(),
  '/goToHome': (context) => const HomePage(),
};
