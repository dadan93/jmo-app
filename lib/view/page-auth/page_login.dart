import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../constant/color_palette.dart';
import '../page-home/widgets/home_widget.dart';
import '../widgets/button_loading_widget.dart';
import '../widgets/flushbar_message.dart';
import '../widgets/input_decoration_widget.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _passwordObscure = true;
  final bool _isLoading = true;
  final _formKey = GlobalKey<FormState>();
  String? version;
  bool _loadVersion = true;
  bool _keyboardVisible = false;
  final String _emailDummy = "dadan@gmail.com";
  final String _passwordDummy = "123";

  void togglePasswordVisibility() {
    setState(() {
      _passwordObscure = !_passwordObscure;
    });
  }

  void _versionInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      version = packageInfo.version;
      _loadVersion = false;
    });
  }

  void _writeData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("name", "Dadan");
    prefs.setString("email", _emailDummy);
    prefs.setString("phone", "081564740600");
    prefs.setString("addres", "Srengseng Kembangan , Jakarta Barat");
  }

  void login(String username, String password) async {
    final form = _formKey.currentState!;
    if (form.validate()) {
      if (username != _emailDummy) {
        flushbarMessage('Email tidak sesuai', Colors.red).show(context);
      } else if (password != _passwordDummy) {
        flushbarMessage('Password tidak sesuai', Colors.red).show(context);
      } else {
        _writeData();
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return const HomeWidget();
        }));
        // _loading(context);
        // Future.delayed(const Duration(seconds: 1), () {
        //   Navigator.push(context, MaterialPageRoute(builder: (context) {
        //     return const HomeWidget();
        //   }));
        // });
      }
    }
  }

  _loading(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) {
        return const AlertDialog(
          insetPadding: EdgeInsets.zero,
          contentPadding: EdgeInsets.zero,
          content: SizedBox(
              width: 100,
              height: 100,
              child: Center(child: CircularProgressIndicator())),
        );
      },
    );
  }

  @override
  void initState() {
    _versionInfo();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _keyboardVisible = MediaQuery.of(context).viewInsets.bottom != 0;
    if (_loadVersion == true) {
      return Container(
        color: Colors.white,
      );
    }
    return Scaffold(
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.30,
                child: Center(
                  child: SizedBox(
                      width: 150,
                      height: 150,
                      child: Image.asset(
                        'assets/icon/jmo.png',
                        fit: BoxFit.fitWidth,
                      )),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "LOGIN",
                      style: Theme.of(context)
                          .textTheme
                          .headlineSmall!
                          .apply(fontWeightDelta: 3),
                    ),
                    const Text("Silahkan login untuk masuk aplikasi",
                        style: TextStyle(fontSize: 14)),
                    const SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: _emailController,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      cursorColor: ColorPalette.activeColor,
                      style: GoogleFonts.poppins(
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Email tidak boleh kosong';
                        }
                        return null;
                      },
                      decoration: inputDecoration('Email Anda'),
                      keyboardType: TextInputType.emailAddress,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: _passwordController,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      cursorColor: ColorPalette.activeColor,
                      style: GoogleFonts.poppins(
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                      ),
                      decoration: inputPasswordDecoration('Kata Sandi'),
                      keyboardType: TextInputType.text,
                      obscureText: _passwordObscure,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Kata sandi tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: () {},
                          child: const Text(
                            "Lupa Akun?",
                            style: TextStyle(color: Colors.red),
                          ),
                        ),
                        InkWell(
                          onTap: () {},
                          child: const Text(
                            "Lupa Kata Sandi?",
                            style: TextStyle(color: Colors.red),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    SizedBox(
                      width: double.infinity,
                      height: 50,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: ColorPalette.blueColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                        onPressed: () {
                          login(
                              _emailController.text, _passwordController.text);
                        },
                        child: _isLoading
                            ? const Text("Login")
                            : const ButtonLoadingWidget(color: Colors.white),
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    InkWell(
                      onTap: () {},
                      child: const Center(
                        child: Text(
                          "Buat Akun",
                          style: TextStyle(color: Colors.green),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                        margin: const EdgeInsets.only(
                            left: 16, right: 16, bottom: 16),
                        width: double.infinity,
                        height: _keyboardVisible == true
                            ? 50
                            : MediaQuery.of(context).size.height * 0.20,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            version == null
                                ? const Text("1.0.0")
                                : Text(version!),
                          ],
                        )),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  InputDecoration inputPasswordDecoration(String hintText) => InputDecoration(
        filled: true,
        fillColor: ColorPalette.colorFilledTextField,
        hintText: 'Min. 6 karakter',
        hintStyle: GoogleFonts.poppins(
          color: ColorPalette.colorHintText,
          fontWeight: FontWeight.w400,
          fontSize: 14,
        ),
        errorStyle: GoogleFonts.poppins(
          color: Colors.red,
          fontWeight: FontWeight.w400,
          fontSize: 12,
        ),
        contentPadding:
            const EdgeInsets.only(left: 12, right: 12, bottom: 8, top: 8),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: ColorPalette.greyColor),
          borderRadius: BorderRadius.circular(5.0),
        ),
        focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: const BorderSide(color: Colors.red)),
        errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: const BorderSide(color: Colors.red)),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: const BorderSide(color: Colors.grey)),
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(5.0),
        ),
        suffixIcon: IconButton(
          icon: Icon(
            _passwordObscure ? Icons.visibility_off : Icons.visibility,
            color: _passwordObscure ? Colors.grey : Colors.green,
          ),
          onPressed: () {
            togglePasswordVisibility();
          },
        ),
      );
}
