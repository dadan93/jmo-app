import 'package:flutter/material.dart';

class SnackBarPage extends StatelessWidget {
  final String? msg;
  const SnackBarPage({Key? key, this.msg}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: snackBarError(context, msg!),
    );
  }

  snackBarError(BuildContext context, String msg) {
    final snackBar = SnackBar(
      content: Text(msg),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
