import 'package:flutter/material.dart';
import 'package:jmo_app/viewmodels/news_vm.dart';
import 'package:provider/provider.dart';

import '../../model/detail_news_model.dart';
import '../../model/news_model.dart';
import '../../utils/api.helper.dart';
import 'page_detail_news.dart';
import 'widgets/appbar.dart';

class NewsPage extends StatefulWidget {
  const NewsPage({super.key});

  @override
  State<NewsPage> createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  bool _isLoading = true;
  NewsModel? _newsData;
  DetailNewsModel? _detailData;

  _fetchData() {
    final CHttp chttp = Provider.of<CHttp>(context, listen: false);
    final NewsViewModel newsVM = NewsViewModel(http: chttp);

    newsVM.fetchNewsData().then((value) {
      setState(() {
        _newsData = value;
        _isLoading = false;
      });
    });
  }

  @override
  void initState() {
    _fetchData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_isLoading == true) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    }
    return Scaffold(
      body: Stack(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height,
            child: Image.asset(
              'assets/images/background.jpg',
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            color: Colors.white.withOpacity(0.8),
          ),
          const AppBarWidget(),
          const SizedBox(
            height: 20,
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.12,
                  left: 16.0,
                  right: 16.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      "Berita Terbaru",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.30,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              height: MediaQuery.of(context).size.height * 0.20,
                              width: double.infinity,
                              decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      topRight: Radius.circular(10)),
                                  color: Colors.white),
                              child: ClipRRect(
                                borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10)),
                                child: _newsData!.articles.first.urlToImage ==
                                        null
                                    ? Image.asset(
                                        'assets/images/default_image.jpg',
                                        fit: BoxFit.cover)
                                    : Image.network(
                                        _newsData!.articles.first.urlToImage!,
                                        fit: BoxFit.fitWidth,
                                        errorBuilder:
                                            (context, error, stackTrace) {
                                          return Container(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.30,
                                            width: double.infinity,
                                            decoration: const BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                    topLeft:
                                                        Radius.circular(10),
                                                    topRight:
                                                        Radius.circular(10)),
                                                color: Colors.white),
                                            child: Image.asset(
                                                'assets/images/default_image.jpg',
                                                fit: BoxFit.cover),
                                          );
                                        },
                                      ),
                              )),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(_newsData!.articles.first.title),
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const Text(
                      "Berita Lainnya",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    GridView.builder(
                      shrinkWrap: true,
                      physics: const ScrollPhysics(),
                      padding: EdgeInsets.zero,
                      gridDelegate:
                          const SliverGridDelegateWithMaxCrossAxisExtent(
                              maxCrossAxisExtent: 200,
                              childAspectRatio: 2 / 2,
                              crossAxisSpacing: 15,
                              mainAxisSpacing: 15),
                      itemCount: _newsData!.articles.length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            setState(() {
                              _detailData = DetailNewsModel(
                                  title: _newsData!.articles[index].title,
                                  description:
                                      _newsData!.articles[index].description,
                                  urlToImage:
                                      _newsData!.articles[index].urlToImage);
                            });
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return DetailNewsPage(
                                data: _detailData,
                              );
                            }));
                          },
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.15,
                            width: double.infinity,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                    height: MediaQuery.of(context).size.height *
                                        0.10,
                                    width: double.infinity,
                                    decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(10),
                                            topRight: Radius.circular(10)),
                                        color: Colors.white),
                                    child: ClipRRect(
                                      borderRadius: const BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10)),
                                      child: _newsData!
                                                  .articles[index].urlToImage ==
                                              null
                                          ? Image.asset(
                                              'assets/images/default_image.jpg',
                                              fit: BoxFit.cover)
                                          : Image.network(
                                              _newsData!
                                                  .articles[index].urlToImage!,
                                              fit: BoxFit.fitWidth,
                                              errorBuilder: (context, error,
                                                      stackTrace) =>
                                                  Image.asset(
                                                      'assets/images/default_image.jpg',
                                                      fit: BoxFit.cover),
                                            ),
                                    )),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child:
                                        Text(_newsData!.articles[index].title),
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      },
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
