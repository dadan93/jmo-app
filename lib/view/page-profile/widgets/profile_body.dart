import 'package:flutter/material.dart';
import 'package:jmo_app/main.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileBody extends StatelessWidget {
  const ProfileBody({
    super.key,
  });

  _logout(BuildContext context) {
    _deleteData();
    _goToLogin(context);
  }

  _deleteData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('name');
  }

  _goToLogin(BuildContext context) {
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) {
      return const MyApp();
    }), (route) => false);
  }

  _showLogoutPopup(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: const Text("Keluar Aplikasi?"),
          actionsPadding: const EdgeInsets.all(16.0),
          actions: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.red[50]),
                    child: const Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text(
                        "Batal",
                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    _logout(context);
                  },
                  child: Container(
                    width: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.green[50]),
                    child: const Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Center(
                        child: Text(
                          "Ya",
                          style: TextStyle(color: Colors.green),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.50,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white.withOpacity(0.8)),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              onTap: () {},
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text("Ubah Profil"),
                    Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: Colors.blue[50]),
                      child:
                          const Icon(Icons.edit, size: 20, color: Colors.blue),
                    )
                  ],
                ),
              ),
            ),
            const Divider(thickness: 2),
            InkWell(
              onTap: () {},
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text("Kartu Digital Anda"),
                    Icon(
                      Icons.keyboard_arrow_right,
                      color: Colors.grey,
                    )
                  ],
                ),
              ),
            ),
            const Divider(thickness: 2),
            InkWell(
              onTap: () {},
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text("Syarat dan Ketentuan"),
                    Icon(
                      Icons.keyboard_arrow_right,
                      color: Colors.grey,
                    )
                  ],
                ),
              ),
            ),
            const Divider(thickness: 2),
            InkWell(
              onTap: () {},
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text("Kebijakan dan Privasi"),
                    Icon(
                      Icons.keyboard_arrow_right,
                      color: Colors.grey,
                    )
                  ],
                ),
              ),
            ),
            const Divider(thickness: 2),
            InkWell(
              onTap: () {},
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text("Tentang Aplikasi"),
                    Icon(
                      Icons.keyboard_arrow_right,
                      color: Colors.grey,
                    )
                  ],
                ),
              ),
            ),
            const Divider(thickness: 2),
            InkWell(
              onTap: () {
                _showLogoutPopup(context);
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "Keluar",
                      style: TextStyle(color: Colors.red),
                    ),
                    Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: Colors.red[50]),
                      child:
                          const Icon(Icons.logout, size: 20, color: Colors.red),
                    )
                  ],
                ),
              ),
            ),
            const Divider(thickness: 2),
          ],
        ),
      ),
    );
  }
}
