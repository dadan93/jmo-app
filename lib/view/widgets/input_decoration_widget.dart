import 'package:jmo_app/constant/color_palette.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

InputDecoration inputDecoration(String hintText) => InputDecoration(
      filled: true,
      fillColor: ColorPalette.colorFilledTextField,
      hintText: hintText,
      hintStyle: GoogleFonts.poppins(
        color: ColorPalette.colorHintText,
        fontWeight: FontWeight.w400,
        fontSize: 14,
      ),
      errorStyle: GoogleFonts.poppins(
        color: Colors.red,
        fontWeight: FontWeight.w400,
        fontSize: 12,
      ),
      contentPadding:
          const EdgeInsets.only(left: 12, right: 12, bottom: 8, top: 8),
      enabledBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: ColorPalette.greyColor),
        borderRadius: BorderRadius.circular(5.0),
      ),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: const BorderSide(color: Colors.red)),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: const BorderSide(color: Colors.red)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: const BorderSide(color: Colors.grey)),
      border: OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadius.circular(5.0),
      ),
    );
InputDecoration inputDecorationWithoutHintText() => InputDecoration(
      filled: true,
      fillColor: ColorPalette.colorFilledTextField,
      contentPadding:
          const EdgeInsets.only(left: 12, right: 12, bottom: 8, top: 8),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: const BorderSide(color: Colors.grey)),
      border: OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.grey),
        borderRadius: BorderRadius.circular(5.0),
      ),
    );
InputDecoration searchInputDecoration(String hintText, Widget icon) =>
    InputDecoration(
      filled: true,
      fillColor: ColorPalette.colorFilledTextField,
      hintText: hintText,
      prefixIcon: icon,
      hintStyle: GoogleFonts.poppins(
        color: ColorPalette.colorHintText,
        fontWeight: FontWeight.w400,
        fontSize: 14,
      ),
      errorStyle: GoogleFonts.poppins(
        color: Colors.red,
        fontWeight: FontWeight.w400,
        fontSize: 12,
      ),
      contentPadding:
          const EdgeInsets.only(left: 12, right: 12, bottom: 8, top: 8),
      enabledBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: ColorPalette.greyColor),
        borderRadius: BorderRadius.circular(20.0),
      ),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0),
          borderSide: const BorderSide(color: Colors.red)),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0),
          borderSide: const BorderSide(color: Colors.red)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0),
          borderSide: const BorderSide(color: ColorPalette.activeColor)),
      border: OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadius.circular(20.0),
      ),
    );
InputDecoration selectInputDecoration(String hintText, Widget icon) =>
    InputDecoration(
      filled: true,
      fillColor: ColorPalette.colorFilledTextField,
      hintText: hintText,
      suffixIcon: icon,
      hintStyle: GoogleFonts.poppins(
        color: ColorPalette.colorHintText,
        fontWeight: FontWeight.w400,
        fontSize: 14,
      ),
      errorStyle: GoogleFonts.poppins(
        color: Colors.red,
        fontWeight: FontWeight.w400,
        fontSize: 12,
      ),
      contentPadding:
          const EdgeInsets.only(left: 12, right: 12, bottom: 8, top: 8),
      enabledBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: ColorPalette.greyColor),
        borderRadius: BorderRadius.circular(10.0),
      ),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: const BorderSide(color: Colors.red)),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: const BorderSide(color: Colors.red)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: const BorderSide(color: ColorPalette.activeColor)),
      border: OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadius.circular(10.0),
      ),
    );
