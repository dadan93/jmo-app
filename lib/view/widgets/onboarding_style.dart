import 'package:flutter/material.dart';

const titleStyle = TextStyle(
  fontSize: 18.0,
  wordSpacing: 1,
  letterSpacing: 1.2,
  fontWeight: FontWeight.bold,
  color: Colors.black,
);
const infoStyle = TextStyle(
  color: Colors.black,
  letterSpacing: 0.7,
  height: 1.5,
);
const buttonTextStyle = TextStyle(
    color: Colors.green, letterSpacing: 1.0, fontWeight: FontWeight.bold);
const nextButtonTextStyle = TextStyle(
    color: Colors.grey, letterSpacing: 1.0, fontWeight: FontWeight.bold);
