import 'package:flutter/material.dart';
import 'package:onboarding/onboarding.dart';

import 'widgets/onboarding_style.dart';

class OnboardingScreen extends StatefulWidget {
  const OnboardingScreen({Key? key}) : super(key: key);

  @override
  State<OnboardingScreen> createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  late Material materialButton;
  late int index;

  static const width = 100.0;

  @override
  void initState() {
    super.initState();
    index = 0;
  }

  _skipButton({void Function(int)? setIndex}) {
    return InkWell(
      borderRadius: defaultSkipButtonBorderRadius,
      onTap: () {
        if (setIndex != null) {
          index = 2;
          setIndex(2);
        }
      },
      child: const Padding(
        padding: defaultSkipButtonPadding,
        child: Text(
          'Lewati',
          style: nextButtonTextStyle,
        ),
      ),
    );
  }

  _nextButton({void Function(int)? setIndex}) {
    return InkWell(
      borderRadius: defaultProceedButtonBorderRadius,
      onTap: () {
        if (setIndex != null) {
          index += 1;
          setIndex(index);
        }
      },
      child: const Padding(
        padding: defaultProceedButtonPadding,
        child: Text(
          'Lebih Lanjut >',
          style: buttonTextStyle,
        ),
      ),
    );
  }

  _signinButton() {
    return SizedBox(
      child: Align(
        alignment: Alignment.centerRight,
        child: InkWell(
          onTap: () {
            Navigator.pushNamed(context, '/login');
          },
          child: const Padding(
            padding: signinButtonPadding,
            child: Text(
              'Lebih Lanjut >',
              style: buttonTextStyle,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final onboardingPagesList = [
      PageModel(
        widget: DecoratedBox(
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              width: 0.0,
              color: Colors.white,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 45.0,
                  vertical: 90.0,
                ),
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.40,
                  child: Image.asset('assets/images/user_onboarding.png',
                      fit: BoxFit.fitHeight),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset:
                            const Offset(0, 3), // changes position of shadow
                      ),
                    ],
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20))),
                child: Padding(
                  padding: const EdgeInsets.only(top: 16),
                  child: Column(
                    children: const [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 45.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Selamat Datang di JMO',
                            style: titleStyle,
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 45.0, vertical: 10.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Kini hadir dengan beragam fitur baru Lebih Lengkap, Lebih Mudah dan Lebih Cepat',
                            style: infoStyle,
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                      Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 45.0, vertical: 0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "",
                            style: infoStyle,
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      PageModel(
        widget: DecoratedBox(
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              width: 0.0,
              color: Colors.white,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 45.0,
                  vertical: 90.0,
                ),
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.40,
                  child: Image.asset('assets/images/user_onboarding.png',
                      fit: BoxFit.fitHeight),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset:
                            const Offset(0, 3), // changes position of shadow
                      ),
                    ],
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20))),
                child: Padding(
                  padding: const EdgeInsets.only(top: 16),
                  child: Column(
                    children: const [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 45.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Pengkinian Data',
                            style: titleStyle,
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 45.0, vertical: 10.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Segera update datamu, gak nyampe 5 menit manfaatnya selangit',
                            style: infoStyle,
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 45.0, vertical: 10.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "",
                            style: infoStyle,
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      PageModel(
        widget: DecoratedBox(
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              width: 0.0,
              color: Colors.white,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 45.0,
                  vertical: 90.0,
                ),
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.40,
                  child: Image.asset('assets/images/user_onboarding.png',
                      fit: BoxFit.fitHeight),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset:
                            const Offset(0, 3), // changes position of shadow
                      ),
                    ],
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20))),
                child: Padding(
                  padding: const EdgeInsets.only(top: 16),
                  child: Column(
                    children: const [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 45.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Klaim JHT via JMO',
                            style: titleStyle,
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 45.0, vertical: 10.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Yes! Klaim JHT pake HP, tanpa unggah dokumen, langsung masuk rekeningmu',
                            style: infoStyle,
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 45.0, vertical: 10.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "",
                            style: infoStyle,
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    ];

    return Scaffold(
      body: Onboarding(
        pages: onboardingPagesList,
        onPageChange: (int pageIndex) {
          index = pageIndex;
        },
        startPageIndex: 0,
        footerBuilder: (context, dragDistance, pagesLength, setIndex) {
          return DecoratedBox(
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                width: 0.0,
                color: Colors.white,
              ),
            ),
            child: ColoredBox(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 16.0, right: 16.0, top: 45.0, bottom: 45.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    index != pagesLength - 1
                        ? _skipButton(setIndex: setIndex)
                        : const SizedBox(width: width),
                    Padding(
                      padding: const EdgeInsets.only(right: 45.0),
                      child: CustomIndicator(
                          netDragPercent: dragDistance,
                          pagesLength: pagesLength,
                          indicator: Indicator(
                            closedIndicator:
                                const ClosedIndicator(color: Colors.green),
                            indicatorDesign: IndicatorDesign.polygon(
                              polygonDesign: PolygonDesign(
                                polygon: DesignType.polygon_circle,
                              ),
                            ),
                          )),
                    ),
                    index != pagesLength - 1
                        ? _nextButton(setIndex: setIndex)
                        : _signinButton(),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
