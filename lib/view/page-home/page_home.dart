import 'package:flutter/material.dart';

import 'widgets/button_program.dart';
import 'widgets/home_appbar.dart';
import 'widgets/menu_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.38,
              child: Stack(
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.38,
                    child: Image.asset(
                      'assets/images/background.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                  const HomeAppBar()
                ],
              ),
            ),
            const ButtonProgramLainnya(),
            const SizedBox(
              height: 30,
            ),
            const MenuWidget(),
          ],
        ),
      ),
    );
  }
}
