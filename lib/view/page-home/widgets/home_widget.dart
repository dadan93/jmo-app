import 'dart:developer';
import 'package:double_back_to_close/double_back_to_close.dart';
import 'package:flutter/material.dart';

import '../../../constant/color_palette.dart';
import '../../page-card/page_digital_card.dart';
import '../../page-news/page_news.dart';
import '../../page-profile/page_profile.dart';
import '../page_home.dart';

class HomeWidget extends StatefulWidget {
  const HomeWidget({Key? key}) : super(key: key);

  @override
  State<HomeWidget> createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  int _currentIndex = 0;
  Map<String, dynamic>? basket;
  final List<Widget> _children = [
    const HomePage(),
    const NewsPage(),
    const DigitalCard(),
    const ProfilePage(),
  ];

  void onTabTapped(int index) {
    log("tapped");
    setState(() {
      _currentIndex = index;
    });
  }

  final List<BottomNavigationBarItem> _items = const [
    BottomNavigationBarItem(
      activeIcon: RadiantGradientMask(
        child: ImageIcon(
          AssetImage("assets/icon/ic_home.png"),
          size: 25,
          color: Colors.white,
        ),
      ),
      icon: ImageIcon(
        AssetImage("assets/icon/ic_home.png"),
        size: 25,
        color: ColorPalette.disableColor,
      ),
      label: 'Beranda',
    ),
    BottomNavigationBarItem(
      activeIcon: RadiantGradientMask(
        child: ImageIcon(
          AssetImage("assets/icon/ic_news.png"),
          size: 25,
          color: Colors.white,
        ),
      ),
      icon: ImageIcon(
        AssetImage("assets/icon/ic_news.png"),
        size: 25,
        color: ColorPalette.disableColor,
      ),
      label: 'Berita',
    ),
    BottomNavigationBarItem(
      activeIcon: RadiantGradientMask(
        child: ImageIcon(
          AssetImage("assets/icon/ic_digital_card.png"),
          size: 25,
          color: Colors.white,
        ),
      ),
      icon: ImageIcon(
        AssetImage("assets/icon/ic_digital_card.png"),
        size: 25,
        color: ColorPalette.disableColor,
      ),
      label: 'Kartu Digital',
    ),
    BottomNavigationBarItem(
        activeIcon: RadiantGradientMask(
          child: ImageIcon(
            AssetImage("assets/icon/ic_profile.png"),
            size: 25,
            color: Colors.white,
          ),
        ),
        icon: ImageIcon(
          AssetImage("assets/icon/ic_profile.png"),
          size: 25,
          color: ColorPalette.disableColor,
        ),
        label: 'Profil Saya'),
  ];

  @override
  Widget build(BuildContext context) {
    return DoubleBack(
      message: 'Tekan tombol kembali lagi untuk keluar',
      child: Scaffold(
        body: _children[_currentIndex], // new
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.white,
          elevation: 10,
          onTap: onTabTapped, // new
          currentIndex: _currentIndex,
          type: BottomNavigationBarType.fixed, // new
          showSelectedLabels: false,
          showUnselectedLabels: false,
          items: _items,
        ),
      ),
    );
  }
}

class RadiantGradientMask extends StatelessWidget {
  final Widget? child;
  const RadiantGradientMask({Key? key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (bounds) => const RadialGradient(
        center: Alignment.topLeft,
        colors: [Colors.green, Colors.green],
        tileMode: TileMode.repeated,
      ).createShader(bounds),
      child: child,
    );
  }
}
