import 'package:flutter/material.dart';

class ButtonProgramLainnya extends StatelessWidget {
  const ButtonProgramLainnya({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 8.0),
      child: ElevatedButton(
          onPressed: () {},
          style: ElevatedButton.styleFrom(
            backgroundColor: Colors.green[100],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
          ),
          child: const SizedBox(
            height: 45,
            width: double.infinity,
            child: Center(
              child: Text(
                "Program Lainnya",
                style: TextStyle(color: Colors.green, fontSize: 16),
              ),
            ),
          )),
    );
  }
}
