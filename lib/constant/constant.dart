const String hostDomain = "";
const String baseUrl = "https://$hostDomain";
const String appTitle = "JMO App";
const String mobileUA =
    "Mozilla/5.0 (Linux; Android 7.0; SM-G930V Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.125 Mobile Safari/537.36";
const String apiKey = "";
