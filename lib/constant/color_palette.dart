import 'package:flutter/material.dart';

class ColorPalette {
  static const activeColor = Color(0x001e4ecb);
  // static const activeColor = Color(0xFFcf0f0f);
  // static const activeColor = Color(0xFFF8A170);
  // static const primaryColor = Color(0xFF283E51);
  static const primaryColor = Color(0xFF0D47A1);
  static const purpleColor = Color(0xFF4910C2);
  static const greenColor = Color(0xFF48B605);
  static const greenButton = Color(0xFF0BBE09);
  static const blueColor = Color(0xFF1E4ECB);
  static const blueColor2 = Color(0xFF317B9B);
  static const blueColor3 = Color(0xFF0852D5);
  static const blueColor4 = Color(0xFF639AFC);
  static const blueCardPermit = Color(0xFFF2F7FA);
  static const disableColor = Color(0xFFDFDEDE);
  static const hintColor = Color(0xFF999999);
  static const blackColor = Color(0xFF393939);
  static const greyColor = Color(0xFF999999);
  static const orangeColor = Color(0xFFcf0f0f);
  static const orangeGradient1 = Color(0xFFcf0f0f);
  static const orangeGradient2 = Color(0xFFcf0f0f);
  static const colorBodyText = Color(0xFF5D5F61);
  static const colorHintText = Color(0xFF989B9D);
  static const colorFilledTextField = Color(0xFFF4F5F6);
  static const blueGradient1 = Color(0xFF1976D2);
  static const blueGradient2 = Color(0xFF0E72E6);
  static const blueGradient3 = Color(0xFF082758);
  // static const blueGradient1 = Color(0xFF283E51);
  // static const blueGradient2 = Color(0xFF4B79A1);
  static const blueCardGradient1 = Color(0xFF0F6B92);
  static const blueCardGradient2 = Color(0xFF148FC3);
  static const greenGradient1 = Color(0xFF8DDC29);
  static const greenGradient2 = Color(0xFF4DBF25);
  static const greenGradient3 = Color(0xFF099639);
  static const redGradient1 = Color(0xFFDC7429);
  static const redGradient2 = Color(0xFFBF4A25);
  static const redGradient3 = Color(0xFF940A0A);
  static const colorBorderCard = Color(0xFFE4EAF0);
  static const kSecondaryColor = Color(0xFF979797);
  static const kTextColor = Color(0xFF757575);
  static const profileSecondaryColor = Color(0xFFEEF7FC);
}

final kBoxDecorationStyle = BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.circular(10.0),
    border: Border.all(color: Colors.grey));
const kHintTextStyle2 = TextStyle(
    color: Colors.grey, fontFamily: 'Poppins', fontWeight: FontWeight.bold);
const borderColor = Color.fromRGBO(23, 171, 144, 0.4);
const focusedBorderColor = Color.fromRGBO(23, 171, 144, 1);
const fillColor = Color.fromRGBO(243, 246, 249, 0);
