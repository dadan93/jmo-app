import 'package:dio/dio.dart';

import '../model/news_model.dart';
import '../utils/api.helper.dart';

class NewsViewModel {
  final CHttp? http;
  NewsViewModel({this.http});

  Future<NewsModel?> fetchNewsData() async {
    try {
      final Dio dio = Dio();
      Response res = await dio.get(
          "https://newsapi.org/v2/everything?q=tesla&from=2023-05-06&sortBy=publishedAt&apiKey=2ab1d054040742499c76976110a1c826");
      if (res.statusCode == 200) {
        final NewsModel results = NewsModel.fromJson(res.data);
        return results;
      }
      print(res.data);
      throw (res.statusCode!);
    } catch (err) {
      return null;
    }
  }
}
