// ignore_for_file: avoid_print

import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jmo_app/constant/color_palette.dart';
import 'package:jmo_app/constant/constant.dart';
import 'package:jmo_app/providers.dart';
import 'package:jmo_app/utils/routes.dart';
import 'package:jmo_app/view/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: const SplashScreen(),
    routes: <String, WidgetBuilder>{
      '/MyApp': (BuildContext context) => const MyApp(),
    },
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);

    super.initState();
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: providers,
      child: MaterialApp(
          title: appTitle,
          debugShowCheckedModeBanner: false,
          initialRoute: '/',
          routes: appRoutes,
          locale: const Locale('id'),
          localizationsDelegates: const [GlobalMaterialLocalizations.delegate],
          theme: ThemeData.light().copyWith(
              // scaffoldBackgroundColor: ColorPalette.colorHintText,
              textTheme:
                  GoogleFonts.poppinsTextTheme(Theme.of(context).textTheme),
              canvasColor: ColorPalette.activeColor,
              pageTransitionsTheme: const PageTransitionsTheme(
                builders: <TargetPlatform, PageTransitionsBuilder>{
                  TargetPlatform.android: ZoomPageTransitionsBuilder(),
                  TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
                },
              ))),
    );
  }
}
